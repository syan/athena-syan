/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FPGATRACKSIMROADCOLLECTION_H
#define FPGATRACKSIMROADCOLLECTION_H

#include "AthContainers/DataVector.h"
#include "AthenaKernel/CLASS_DEF.h"
#include "FPGATrackSimObjects/FPGATrackSimRoad.h"

typedef std::vector<FPGATrackSimRoad> FPGATrackSimRoadCollection;
CLASS_DEF( FPGATrackSimRoadCollection , 1320348220 , 1 )

#endif // FPGATRACKSIMROADCOLLECTION_DEF_H
