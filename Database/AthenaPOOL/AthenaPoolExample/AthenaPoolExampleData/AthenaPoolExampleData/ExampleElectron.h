/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
 */

#ifndef ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRON_H
#define ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRON_H

#include "AthenaPoolExampleData/versions/ExampleElectron_v1.h"

namespace xAOD {

typedef ExampleElectron_v1 ExampleElectron;

}  // namespace xAOD

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF(xAOD::ExampleElectron, 252593077, 1)

#endif  // ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRON_H
